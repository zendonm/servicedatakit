//
//  HomeVC.m
//  ServiceDataKit
//
//  Created by ZenDo on 2/6/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "HomeVC.h"
#import "OARequest.h"
#import "DNConnectionOperationQueue.h"
#import "DNCache.h"
#import "NSString+Encrypt.h"
#import "API.h"
#import "ItemCell.h"

@interface HomeVC () <CellDelegate>
{
    __weak IBOutlet UITableView *_table;
    __weak IBOutlet UITextView *_textView;
    BOOL _isCompletedRefreshing;
}

@end

@implementation HomeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isCompletedRefreshing) {
        return 3;
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    
    cell.delegate = self;
    
    NSString *urlPath = @"https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-frc3/t31/1398492_169964063199042_210835625_o.jpg";
    
    NSString *cachedPath = [[DNCache sharedCache] filePathOfURLPath:urlPath];
    if (cachedPath) {
        cell.photo.image = [UIImage imageWithContentsOfFile:cachedPath];
    }
    else if ([[DNCache sharedCache] cacheStateForURLPath:urlPath] == CacheURLStateFailed) {
        cell.photo.image = nil;
    }
    else {
        cell.sumary.text = @"Downloading...";
        cell.photo.image = nil;
        [[DNCache sharedCache] cacheURLPath:urlPath completion:^(NSString *filePath) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            });
        } retry:NO];
    }
    return cell;
}

- (void)_showDetailOfItemAtCell:(UITableViewCell *)cell
{
    //NSIndexPath *path = [_table indexPathForCell:cell];
}

- (IBAction)_load:(id)sender
{
    [self _beginRefreshing];
}

- (void)_beginRefreshing
{
    [self _endRefreshing];
//    _isCompletedRefreshing = NO;
//    [_table reloadData];
//
//    OARequest *request = [[OARequest alloc] initWithMethod:@"POST" host:HOST endPoint:END_POINT path:@"/users/auth"
//                                                      args:@"{\"device_uuid\":\"12345679\",\"bus_email\":\"thelinh299@gmail.com\",\"bus_password\":\"111111\"}"
//                                                 publicKey:APPLICATION_KEY privateKey:SECRET_KEY encoding:YES];
//    
//    DNConnectionOperation *operation = [[DNConnectionOperation alloc] init];
//    operation.request = request;
//    [operation setCompletion:^(DNConnectionOperation *op, NSError *err) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (err) {
//                NSString *msg = [NSString stringWithFormat:@"%@", err.localizedDescription];
//                NSLog(@"%@", msg);
//                _textView.text = msg;
//            }
//            else {
//                NSData *base64Data = op.data;
//                NSString *base64String = [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
//                NSLog(@"---> %@", base64String);
//                NSString *string = [base64String base64Decode];
//                NSString *xorString = [string xorWithKey:SECRET_KEY];
//                _textView.text = xorString;
//            }
//            
//            [self _endRefreshing];
//        });
//    }];
//    
//    [[DNConnectionOperationQueue sharedQueue] addOperation:operation];
}

- (void)_endRefreshing
{
    _isCompletedRefreshing = YES;
    [_table reloadData];
}

- (IBAction)_clear:(id)sender
{
    _textView.text = @"";
    
    [[DNCache sharedCache] clear];
}

@end
