//
//  NSString+Encrypt.m
//  ServiceOperator
//
//  Created by ZenDo on 1/7/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "NSString+Encrypt.h"

const unsigned int CC_DIGEST_LENGTHs[] = {
    CC_SHA1_DIGEST_LENGTH,
    CC_MD5_DIGEST_LENGTH,
    CC_SHA256_DIGEST_LENGTH,
    CC_SHA384_DIGEST_LENGTH,
    CC_SHA512_DIGEST_LENGTH,
    CC_SHA224_DIGEST_LENGTH
};

@implementation NSString (Encrypt)

- (NSString *)encryptWithAlgorithm:(NSAlgorithm)alg
{
    const char *input = [self UTF8String];
    
    unsigned int digestLen = CC_DIGEST_LENGTHs[alg];
    unsigned char digest[digestLen];
    
    switch (alg) {
        case SHA1:
            CC_SHA1(input, (CC_LONG)strlen(input), digest);
            break;
        case MD5:
            CC_MD5(input, (CC_LONG)strlen(input), digest);
            break;
        case SHA256:
            CC_SHA256(input, (CC_LONG)strlen(input), digest);
            break;
        case SHA384:
            CC_SHA384(input, (CC_LONG)strlen(input), digest);
            break;
        case SHA512:
            CC_SHA512(input, (CC_LONG)strlen(input), digest);
            break;
        case SHA224:
            CC_SHA224(input, (CC_LONG)strlen(input), digest);
            break;
    }
    
    NSMutableString *output = [NSMutableString stringWithCapacity:digestLen * 2];
    
    for (int i = 0; i < digestLen; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

- (NSString *)HmacWithAlgorithm:(NSAlgorithm)alg key:(NSString *)aKey
{
    const char *key  = [[aKey dataUsingEncoding:NSUTF8StringEncoding] bytes];
    const char *input = [[self dataUsingEncoding:NSUTF8StringEncoding] bytes];
    
    unsigned int digestLen = CC_DIGEST_LENGTHs[alg];
    unsigned char digest[digestLen];
    
    CCHmac(alg, key, strlen(key), input, strlen(input), digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

- (NSData *)base64Data
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    const uint8_t* input = (const uint8_t*)[data bytes];
    NSInteger length = [data length];
    
    static char table[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    
    NSMutableData* base64Data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)base64Data.mutableBytes;
    
    NSInteger i;
    for (i = 0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return base64Data;
}

- (NSString *)base64Encode
{
    NSData *base64Data = [self base64Data];
    return [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
}

- (NSString *)base64Decode
{
    NSData *data = [[NSData alloc] initWithBase64EncodedString:self options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (NSString *)xorWithKey:(NSString *)key
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    NSUInteger len = data.length;
    NSUInteger keyLen = keyData.length;
    
    char *bytes = (char *)[data bytes];
    char *keyBytes = (char *)[keyData bytes];
    
    char *result = malloc(len);
    for (int i = 0; i < len; i++) {
        result[i] = bytes[i] ^ keyBytes[i % keyLen];
    }
    return [[NSString alloc] initWithBytes:result length:len encoding:NSUTF8StringEncoding];
}

@end
