//
//  DNDOperation_Private.h
//  ServiceDataKit
//
//  Created by ZenDo on 3/13/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "DNConnectionOperation.h"

@interface DNConnectionOperation ()

+ (NSThread *)networkThread;
+ (BOOL)networkThreadIsAvailable;
+ (void)endNetworkThread;

@end
