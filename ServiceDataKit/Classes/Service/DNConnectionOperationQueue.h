//
//  DNDQueue.h
//  ServiceDataKit
//
//  Created by ZenDo on 3/10/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DNConnectionOperation.h"

@interface DNConnectionOperationQueue : NSOperationQueue

@property (nonatomic, assign) BOOL handleNetworkActivityIndicator;
@property (nonatomic, assign) BOOL handleBackgroundTask;

+ (DNConnectionOperationQueue *)sharedQueue;

@end
