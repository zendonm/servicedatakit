//
//  OARequest.h
//  ServiceDataKit
//
//  Created by ZenDo on 1/23/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OARequest : NSMutableURLRequest

- (id)initWithMethod:(NSString *)method
                host:(NSString *)host
            endPoint:(NSString *)endPoint
                path:(NSString *)path
                args:(NSString *)args
           publicKey:(NSString *)publicKey
          privateKey:(NSString *)privateKey
            encoding:(BOOL)encoding;

@property (nonatomic, strong, readonly) NSString *method;
@property (nonatomic, strong, readonly) NSString *host;
@property (nonatomic, strong, readonly) NSString *endPoint;
@property (nonatomic, strong, readonly) NSString *path;
@property (nonatomic, strong, readonly) NSString *args;
@property (nonatomic, strong, readonly) NSString *publicKey;
@property (nonatomic, strong, readonly) NSString *privateKey;
@property (nonatomic) BOOL encoding;

+ (void)setTimeout:(NSTimeInterval)timeout;

@end
