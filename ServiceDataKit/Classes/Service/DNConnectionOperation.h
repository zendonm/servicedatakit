//
//  DNDownloadOperation.h
//  ServiceDataKit
//
//  Created by ZenDo on 3/10/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DNConnectionOperation : NSOperation <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSURLRequest *request;
@property (nonatomic, strong, readonly) NSURLConnection *connection;
@property (nonatomic, strong, readonly) NSMutableData *data;
@property (nonatomic, strong, readonly) NSError *error;

- (void)setCompletion:(void(^)(DNConnectionOperation *op, NSError *error))completion;

@end
