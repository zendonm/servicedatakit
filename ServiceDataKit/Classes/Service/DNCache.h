//
//  DNCache.h
//  ServiceDataKit
//
//  Created by ZenDo on 2/7/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

/* Using this class for cache small image */

#import <Foundation/Foundation.h>

typedef enum {
    CacheURLStateCached,
    CacheURLStateCaching,
    CacheURLStateFailed
} CacheURLStates;

@interface DNCache : NSObject

- (id)initWithCacheName:(NSString *)cacheName;
@property (nonatomic, strong, readonly) NSString *cachePath;

- (void)cacheURLPath:(NSString *)urlPath
          completion:(void(^)(NSString *filePath))completion
               retry:(BOOL)retry;
- (CacheURLStates)cacheStateForURLPath:(NSString *)urlPath;
- (NSString *)filePathOfURLPath:(NSString *)urlPath;
- (BOOL)clear;

+ (DNCache *)sharedCache;

@end
