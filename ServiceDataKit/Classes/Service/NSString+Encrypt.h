//
//  NSString+Encrypt.h
//  ServiceOperator
//
//  Created by ZenDo on 1/7/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>

typedef enum {
    SHA1,
    MD5,
    SHA256,
    SHA384,
    SHA512,
    SHA224
} NSAlgorithm;

@interface NSString (Encrypt)

- (NSString *)encryptWithAlgorithm:(NSAlgorithm)alg;
- (NSString *)HmacWithAlgorithm:(NSAlgorithm)alg key:(NSString *)key;

- (NSData *)base64Data;
- (NSString *)base64Encode;
- (NSString *)base64Decode;
- (NSString *)xorWithKey:(NSString *)key;

@end
