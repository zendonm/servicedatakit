//
//  NSDictionary+null.m
//  ServiceDataKit
//
//  Created by ZenDo on 1/23/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "NSDictionary+NULL.h"
#import <objc/runtime.h>

@implementation NSDictionary (null)

+ (void)load
{
    method_exchangeImplementations(
                                   class_getInstanceMethod(self, @selector( objectForKey:)),
                                   class_getInstanceMethod(self, @selector(_objectForKey:))
                                   );
}

- (id)_objectForKey:(id)aKey
{
    id obj = [self _objectForKey:aKey];
    if ([obj isEqual:[NSNull null]]) {
        return nil;
    }
    return obj;
}

@end
