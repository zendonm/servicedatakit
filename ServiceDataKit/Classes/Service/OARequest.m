//
//  OARequest.m
//  ServiceDataKit
//
//  Created by ZenDo on 1/23/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "OARequest.h"
#import "NSString+Encrypt.h"

static NSTimeInterval timeout = 60;

@interface OARequest ()

@end

@implementation OARequest

#pragma mark - CLASS

+ (void)setTimeout:(NSTimeInterval)timeout_
{
    timeout = timeout_;
}

#pragma mark - INSTANCE

- (id)initWithMethod:(NSString *)method
                host:(NSString *)host
            endPoint:(NSString *)endPoint
                path:(NSString *)path
                args:(NSString *)args
           publicKey:(NSString *)publicKey
          privateKey:(NSString *)privateKey
            encoding:(BOOL)encoding
{
    self = [super init];
    if (self) {
        _method = method;
        _host = host;
        _endPoint = endPoint;
        _path = path;
        _args = args;
        _publicKey = publicKey;
        _privateKey = privateKey;
        _encoding = encoding;
        
        NSParameterAssert(_host != nil);
        NSParameterAssert(_endPoint != nil);
        NSParameterAssert(_path != nil);
        NSParameterAssert(_args != nil);
        NSParameterAssert(_publicKey != nil);
        NSParameterAssert(_privateKey != nil);
        
        [self _init];
    }
    return self;
}

- (void)_init
{
    self.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    self.timeoutInterval = timeout;
    
    NSString *urlString = [NSString stringWithFormat:@"http://%@%@%@", _host, _endPoint, _path];
    self.URL = [NSURL URLWithString:urlString];
    
    self.HTTPMethod = _method;
    [self setValue:_publicKey forHTTPHeaderField:@"Application-Key"];
    
    NSTimeInterval interval1970 = [[NSDate date] timeIntervalSince1970];
    NSString *timestamp = [NSString stringWithFormat:@"%f", interval1970]; //[@(interval1970) stringValue];
    [self setValue:timestamp forHTTPHeaderField:@"Timestamp"];
    
    [self setHTTPBody:[[self body] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *sign = [NSString stringWithFormat:@"%@&%@&%@&%@&%@", _method, _host, _path, timestamp, _args];
    NSString *sign256 = [sign HmacWithAlgorithm:SHA256 key:_privateKey];
    [self setValue:[sign256 base64Encode] forHTTPHeaderField:@"Signature"];
}

- (NSString *)body
{
    if (_encoding) {
        return [[_args xorWithKey:_privateKey] base64Encode];
    } else {
        return _args;
    }
}

@end
