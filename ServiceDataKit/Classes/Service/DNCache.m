//
//  DNCache.m
//  ServiceDataKit
//
//  Created by ZenDo on 2/7/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "DNCache.h"
#import "DNConnectionOperationQueue.h"
#import "NSString+Encrypt.h"

#define CacheInfo @"Info.plist"

@interface DNCache ()

@property (nonatomic, strong) NSMutableDictionary *cacheStates;
@property (nonatomic, strong) NSMutableDictionary *cacheCompletions;

@end

@implementation DNCache

- (id)initWithCacheName:(NSString *)cacheName
{
    self = [super init];
    if (self) {
        NSString *libCachePath = [@"~/Library/Caches" stringByExpandingTildeInPath];
        
        if (cacheName.length > 0) {
            _cachePath = [libCachePath stringByAppendingPathComponent:cacheName];
        }
        else {
            _cachePath = libCachePath;
        }
        NSLog(@"DNCache - Cache directory: %@", _cachePath);
        
        [self _clean];
    }
    return self;
}

- (NSMutableDictionary *)cacheStates
{
    if (_cacheStates) {
        return _cacheStates;
    }
    
    NSString *cacheInfoPath = [self.cachePath stringByAppendingPathComponent:CacheInfo];
    
    [self willChangeValueForKey:@"cacheStates"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:cacheInfoPath]) {
        _cacheStates = [[NSMutableDictionary alloc] initWithContentsOfFile:cacheInfoPath];
    }
    else {
        _cacheStates = [[NSMutableDictionary alloc] init];
    }
    
    [self didChangeValueForKey:@"cacheStates"];
    
    return _cacheStates;
}

- (NSMutableDictionary *)cacheCompletions
{
    if (_cacheCompletions) {
        return _cacheCompletions;
    }
    _cacheCompletions = [[NSMutableDictionary alloc] init];
    return _cacheCompletions;
}

- (void)cacheURLPath:(NSString *)urlPath completion:(void (^)(NSString *))completion retry:(BOOL)retry
{
    CacheURLStates state = [self cacheStateForURLPath:urlPath];

    if (state == CacheURLStateCaching) {
        [self _addCompletion:completion forURLPath:urlPath];
        return;
    }
    
    if (state == CacheURLStateFailed && !retry) {
        return;
    }
    
    [self _setCacheState:CacheURLStateCaching forURLPath:urlPath];
    [self _setCompletion:completion forURLPath:urlPath];
    
    DNConnectionOperation *operation = [[DNConnectionOperation alloc] init];
    operation.url = [NSURL URLWithString:urlPath];
    [operation setCompletion:^(DNConnectionOperation *op, NSError *error) {
        NSString *filePath = nil;
        if (error) {
            [self _setCacheState:CacheURLStateFailed forURLPath:urlPath];
        }
        else {
            filePath = [self _filePathOfURLPath:urlPath];
            
            if ([op.data writeToFile:filePath atomically:YES]) {
                [self _setCacheState:CacheURLStateCached forURLPath:urlPath];
            }
            else {
                [self _setCacheState:CacheURLStateFailed forURLPath:urlPath];
                filePath = nil;
            }
        }
        
        [self _fireCompletionsForURLPath:urlPath cachedPath:filePath];
    }];
    
    [[DNConnectionOperationQueue sharedQueue] addOperation:operation];
}

- (CacheURLStates)cacheStateForURLPath:(NSString *)urlPath
{
    return [[self.cacheStates objectForKey:urlPath] intValue];
}

- (void)_setCacheState:(CacheURLStates)state forURLPath:(NSString *)urlPath
{
    [self willChangeValueForKey:@"cacheStates"];
    
    if (state == CacheURLStateCached) {
        [self.cacheStates removeObjectForKey:urlPath];
    }
    else {
        [self.cacheStates setObject:@(state) forKey:urlPath];
    }
    
    [self didChangeValueForKey:@"cacheStates"];
    
    NSString *cacheInfoPath = [self.cachePath stringByAppendingPathComponent:CacheInfo];
    [self.cacheStates writeToFile:cacheInfoPath atomically:YES];
}

- (void)_setCompletion:(void(^)())completion forURLPath:(NSString *)urlPath
{
    [self.cacheCompletions setObject:[@[completion] mutableCopy] forKey:urlPath];
}

- (void)_addCompletion:(void(^)())completion forURLPath:(NSString *)urlPath
{
    NSMutableArray *completions = [self.cacheCompletions objectForKey:urlPath];
    [completions addObject:completion];
}

- (void)_fireCompletionsForURLPath:(NSString *)urlPath cachedPath:(NSString *)cachedPath
{
    NSArray *completions = [self.cacheCompletions objectForKey:urlPath];
    [self.cacheCompletions removeObjectForKey:urlPath];
    for (void(^completion)(NSString *) in completions) {
        completion(cachedPath);
    }
}

- (NSString *)filePathOfURLPath:(NSString *)urlPath
{
    NSString *filePath = [self _filePathOfURLPath:urlPath];
    CacheURLStates state = [self cacheStateForURLPath:urlPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:filePath] && state != CacheURLStateCaching) {
        return filePath;
    } else {
        return nil;
    }
}

- (NSString *)_filePathOfURLPath:(NSString *)urlPath
{
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isExist = [fileManager fileExistsAtPath:self.cachePath
                                     isDirectory:&isDir];
    if (!isExist || !isDir) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtPath:self.cachePath withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"DNCache - Create cache directory '%@' successfully.", self.cachePath);
        } else {
            NSLog(@"DNCache - Cannot create directory '%@\'. Error: %@.", self.cachePath, error.localizedDescription);
            return nil;
        }
    }
    
    NSString *name = [urlPath encryptWithAlgorithm:MD5];
    NSString *ext = [urlPath pathExtension];
    NSString *filename = [NSString stringWithFormat:@"%@.%@", name, ext];
    NSString *filePath = [self.cachePath stringByAppendingPathComponent:filename];
    return filePath;
}

- (void)_clean
{
    _cacheStates = nil;
    NSString *cacheInfoPath = [self.cachePath stringByAppendingPathComponent:@"Info.plist"];
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager removeItemAtPath:cacheInfoPath error:&error]) {
        NSLog(@"DNCache - Clean cache info successfully.");
    } else {
        NSLog(@"DNCache - Clean cache info failed with error: %@", error.localizedDescription);
    }
}

- (BOOL)clear
{
    [self willChangeValueForKey:@"cacheStates"];
    self.cacheStates = nil;
    [self didChangeValueForKey:@"cacheStates"];
    self.cacheCompletions = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager removeItemAtPath:self.cachePath
                                error:&error]) {
        return YES;
    } else {
        NSLog(@"DNCache - Clear cache \"%@\" failed with error: %@", [self.cachePath lastPathComponent], [error localizedDescription]);
        return NO;
    }
}

+ (DNCache *)sharedCache
{
    static DNCache *cache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cache = [[DNCache alloc] initWithCacheName:@"com.zendo.cache"];
    });
    return cache;
}

@end
