//
//  DNDownloadOperation.m
//  ServiceDataKit
//
//  Created by ZenDo on 3/10/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "DNConnectionOperation.h"
#import "DNConnectionOperation_Private.h"

@interface DNConnectionOperation ()
{
    BOOL _isExecuting;
    BOOL _isCancelled;
    BOOL _isFinished;
}

@end

@implementation DNConnectionOperation

- (void)setCompletionBlock:(void (^)(void))block
{
    if (!block) {
        [super setCompletionBlock:NULL];
    } else {
        __weak __typeof(self) self_weak = self;
        
        [super setCompletionBlock:^ {
            __strong __typeof(&*self_weak) self_strong = self_weak;
            
            block();
            [self_strong setCompletionBlock:NULL];
        }];
    }
}

- (void)setCompletion:(void (^)(DNConnectionOperation *, NSError *))completion
{
    __weak __typeof(self) self_weak = self;
    
    [self setCompletionBlock:^{
        __strong __typeof(&*self_weak) _self = self_weak;
        
        if ([_self isCancelled]) {
            NSDictionary *errorInfo = @{NSLocalizedDescriptionKey:@"The operation was cancelled."};
            NSError *error = [NSError errorWithDomain:@"vn.zendo.operation.connection" code:NSURLErrorCancelled userInfo:errorInfo];
            completion(_self, error);
        }
        else if (_self.error) {
            if (completion && _self.error) {
                completion(_self, _self.error);
            }
        }
        else {
            if (completion) {
                completion(_self, nil);
            }
        }
    }];
}

- (void)start
{
    if ([self isReady]) {
        [self performSelector:@selector(main) onThread:[[self class] networkThread] withObject:nil waitUntilDone:NO];
    }
    else {
        NSLog(@"ERROR: Cannot start operation: Operation is not ready to start");
    }
}

- (void)main
{
    _error = nil;
    _isFinished = NO;
    _isCancelled = NO;
    
    [self willChangeValueForKey:@"isExecuting"];
    _isExecuting = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    [self startConnection];
}

- (void)startConnection
{
    if (!_request) {
        NSParameterAssert(_url);
        _request = [NSURLRequest requestWithURL:_url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    }
    
    if ([_request isKindOfClass:[NSMutableURLRequest class]]) {
        
    }
    else {
        NSLog(@"%@", _request);
    }
    
    
    _data = [[NSMutableData alloc] init];
    _connection = [[NSURLConnection alloc] initWithRequest:_request
                                                  delegate:self
                                          startImmediately:NO];
    [_connection scheduleInRunLoop : [NSRunLoop currentRunLoop]
                           forMode : NSRunLoopCommonModes];
    [_connection start];
}

#pragma mark - Connection handle

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    _connection = nil;
    _error = error;
    [self completeOperation];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _connection = nil;
    [self completeOperation];
}

- (void)connection:(NSURLConnection *)connection
    didReceiveData:(NSData *)data
{
    [_data appendData:data];
}

- (void)connection:(NSURLConnection *)connection
didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"RESPONSE: %@", [response description]);
}

#pragma mark - Completion

- (void)cancel
{
    if ([self isExecuting] == NO) {
        NSLog(@"ERROR: Cannot Cancel Operation: Operation is not executing");
        return;
    }
    
    BOOL isNetworkThread = ([NSThread currentThread] == [[self class] networkThread]);
    
    // cancelConnection must be called in network thread
    if (isNetworkThread) {
        [self cancelConnection];
    } else {
        [self performSelector:@selector(cancelConnection)
                     onThread:[[self class] networkThread]
                   withObject:nil
                waitUntilDone:NO];
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    _isExecuting = NO;
    _isFinished = YES;
    _isCancelled = YES;
    [super cancel];
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)completeOperation
{
    if ([self isExecuting] == NO) {
        NSLog(@"ERROR: Cannot complete operation: Operation is not executing");
        return;
    }
    
    [self cancelConnection];
    
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    _isFinished = YES;
    _isExecuting = NO;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)cancelConnection // must be called in network thread
{
    [_connection cancel];
    _connection = nil;
}

#pragma mark - States

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return _isExecuting;
}

- (BOOL)isFinished
{
    return _isFinished;
}

#pragma mark - Network Thread

+ (void)networkThreadEntryPoint:(id)object
{
    @autoreleasepool {
        [[NSThread currentThread] setName:@"DNConnectionOperation"];
        do {
            [[NSRunLoop currentRunLoop] run];
        }
        while (YES);
    }
}

static __strong NSThread *networkThread = nil;

+ (NSThread *)networkThread
{
    if (!networkThread) {
        networkThread = [[NSThread alloc] initWithTarget:self
                                                selector:@selector(networkThreadEntryPoint:)
                                                  object:nil];
        [networkThread start];
    }
    return networkThread;
}

+ (void)endNetworkThread
{
    if (!networkThread) {
        return;
    }
    
    if ([NSThread currentThread] == networkThread) {
        [NSThread exit];
    }
    else {
        [NSThread performSelector:@selector(exit)
                         onThread:networkThread
                       withObject:nil
                    waitUntilDone:NO];
    }
    
    networkThread = nil;
}

+ (BOOL)networkThreadIsAvailable
{
    return (networkThread != nil);
}

@end
