//
//  DNDQueue.m
//  ServiceDataKit
//
//  Created by ZenDo on 3/10/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "DNConnectionOperationQueue.h"
#import "DNConnectionOperation.h"
#import "DNConnectionOperation_Private.h"

@interface DNConnectionOperationQueue ()
{
    UIBackgroundTaskIdentifier _bgTask;
    BOOL _running;
}

+ (NSThread *)networkThreadIfAvailable;

@end

@implementation DNConnectionOperationQueue

#pragma mark - CLASS

+ (NSThread *)networkThreadIfAvailable
{
    return ([DNConnectionOperation networkThreadIsAvailable] ? [DNConnectionOperation networkThread] : nil);
}

#pragma mark - INSTANCE

- (id)init
{
    self = [super init];
    if (self) {
        [self addObserver:self forKeyPath:@"operations" options:0 context:NULL];
        self.maxConcurrentOperationCount = 5;
        self.handleBackgroundTask = NO;
        self.handleNetworkActivityIndicator = YES;
    }
    return self;
}

- (void)startBackgroundTask
{
    if (!self.handleBackgroundTask) {
        return;
    }
    _running = YES;
    _bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self cancelAllOperations];
    }];
}

- (void)stopBackgroundTaskAndNetworkThread
{
    [[UIApplication sharedApplication] endBackgroundTask:_bgTask];
    _running = NO;
    [DNConnectionOperation endNetworkThread];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self && [keyPath isEqualToString:@"operations"]) {
        if (self.handleNetworkActivityIndicator) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:(self.operationCount > 0)];
        }
        if (self.operationCount > 0 && !_running) {
            [self startBackgroundTask];
        }
        else if (!self.operationCount && _running) {
            [self stopBackgroundTaskAndNetworkThread];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)addOperation:(NSOperation *)op
{
    if ([op isKindOfClass:[DNConnectionOperation class]]) {
        [super addOperation:op];
    } else {
        NSLog(@"Error: DNConnectionOperationQueue should only be used for enqueing DNConnectionOperation objects. Ignoring");
    }
}

+ (DNConnectionOperationQueue *)sharedQueue
{
    static DNConnectionOperationQueue *sSharedQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sSharedQueue = [[DNConnectionOperationQueue alloc] init];
    });
    return sSharedQueue;
}

@end
