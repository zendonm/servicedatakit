//
//  DataManager.h
//  CoreData
//
//  Created by ZenDo on 7/23/13.
//  Copyright (c) 2013 Nikmesoft Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define CDStoreName @"MeeTu.sqlite"
#define CDStoreType NSSQLiteStoreType
#define CDModelName @"MeeTu"
#define CDModelVersion @"CDModelVersion"

@protocol DataManagerDelegate;

@interface DataManager : NSObject

+ (DataManager *)sharedManager;

@property (nonatomic, weak) NSObject <DataManagerDelegate> *delegate;

@property (nonatomic, strong, readonly) NSManagedObjectContext *mainContext;
@property (nonatomic, strong, readonly) NSManagedObjectContext *writeContext;
@property (nonatomic, strong, readonly) NSManagedObjectModel *model;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *coordinator;

- (NSManagedObjectContext *)newPrivateContext;

- (void)saveWriteContext;
- (void)saveMainContext;
- (void)saveContext:(NSManagedObjectContext *)context;
- (void)writeToDisk;
- (void)performBlock:(void(^)())block target:(id)target async:(BOOL)async;

@end

@protocol DataManagerDelegate <NSObject>
@required
- (void)dataManager:(DataManager *)dataManager willSaveContext:(NSManagedObjectContext *)context;
@end
