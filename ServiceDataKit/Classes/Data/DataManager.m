//
//  DataManager.m
//  CoreData
//
//  Created by ZenDo on 7/23/13.
//  Copyright (c) 2013 Nikmesoft Co., Ltd. All rights reserved.
//

#import "DataManager.h"

#define kTagAlertSerious 666

NSURL *DocDir();
NSURL *CacheDir();

@interface DataManager () <UIAlertViewDelegate>

- (void)_seriousError:(NSString *)message;

@end

@implementation DataManager

@synthesize mainContext = _mainContext;
@synthesize writeContext = _writeContext;
@synthesize model = _model;
@synthesize coordinator = _coordinator;

+ (id)sharedManager
{
    static DataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[DataManager alloc] init];
    });
    return sharedManager;
}

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willSave:) name:NSManagedObjectContextWillSaveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSave:) name:NSManagedObjectContextDidSaveNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// Only run once
//- (void)update
//{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        DataManager *defaultManager = [DataManager mainManager];
//        NSString *savedModelVersion = [DataManager modelVersion];
//        
//        NSManagedObjectModel *model = [defaultManager managedObjectModel];
//        NSArray *versionIdentifiers = [[model versionIdentifiers] allObjects];
//        NSString *currentModelVersion = [versionIdentifiers lastObject];
//        
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:currentModelVersion forKey:CDModelVersion];
//        [userDefaults synchronize];
//        
//        if (savedModelVersion == nil ||
//            [savedModelVersion isEqualToString:currentModelVersion]) {
//            NSLog(@"Current Data Model version: %@", currentModelVersion);
//        } else {
//            NSLog(@"Old Data Model version: %@", savedModelVersion);
//            NSLog(@"Did update Data Model");
//            NSLog(@"New Data Model version: %@", currentModelVersion);
//        }
//    });
//}
//
//- (NSString *)modelVersion
//{
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    NSString *modelVersion = [userDefaults objectForKey:CDModelVersion];
//    return modelVersion;
//}

- (NSManagedObjectContext *)mainContext
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _mainContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _mainContext.parentContext = self.writeContext;
    });
    return _mainContext;
}

- (NSManagedObjectContext *)writeContext
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _writeContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _writeContext.undoManager = nil;
        _writeContext.persistentStoreCoordinator = self.coordinator;
    });
    return _writeContext;
}

- (NSManagedObjectModel *)model
{
    if (_model) {
        return _model;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:CDModelName withExtension:@"momd"];
    _model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _model;
}

- (NSPersistentStoreCoordinator *)coordinator
{
    if (_coordinator) {
        return _coordinator;
    }
    
    NSURL *storeURL = [DocDir() URLByAppendingPathComponent:CDStoreName];
    
    NSError *error = nil;
    NSDictionary *storeMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeURL error:&error];
    
    if (storeMetadata == nil) {
        [self _seriousError:[error localizedDescription]];
        return nil;
    }
    
    NSPersistentStoreCoordinator *psc = nil;
    
    BOOL compatible = [self.model isConfiguration:nil compatibleWithStoreMetadata:storeMetadata];
    
    if (compatible) {
        NSLog(@"Core Data - Model is compatible with Store.");
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model];
        NSPersistentStore *ps = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
        if (ps == nil || error) {
            [self _seriousError:[error localizedDescription]];
            return nil;
        }
    }
    else {
        NSLog(@"Core Data - Model not compatible with Store");
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                                 nil];
        
        NSPersistentStore *ps = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error];
        if (ps && !error) {
            NSLog(@"Core Data - perform Lightweight Migration successful.");
        }
        else {
            NSLog(@"Core Data - perform Lightweight Migration error: %@", [error localizedDescription]);
            
            NSManagedObjectModel *oldModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:storeMetadata];
            
            if (oldModel == nil) {
                [self _seriousError:@"Unsolved serious error with database, please reinstall application!"];
            }
            
            NSMigrationManager *migrationManager = [[NSMigrationManager alloc] initWithSourceModel:oldModel destinationModel:self.model];
            NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:oldModel destinationModel:self.model];
            
            if (mappingModel == nil) {
                [self _seriousError:@"Unsolved serious error with database, please reinstall application!"];
                return nil;
            }
            
            NSURL *tmpStoreURL = [CacheDir() URLByAppendingPathComponent:CDStoreName];
            
            error = nil;
            BOOL success = [migrationManager migrateStoreFromURL:storeURL type:CDStoreType options:nil withMappingModel:mappingModel toDestinationURL:tmpStoreURL destinationType:CDStoreType destinationOptions:nil error:&error];
            if (success) {
                // Replace store by new file.
                [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
                [[NSFileManager defaultManager] moveItemAtURL:tmpStoreURL toURL:storeURL error:nil];
                [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:nil];
                NSLog(@"Core Data - perform Mapping Migration successful.");
            }
            else {
                [self _seriousError:error.localizedDescription];
                return nil;
            }
        }
    }
    
    _coordinator = psc;
    return _coordinator;
}

#pragma mark - Process

- (void)writeToDisk
{
    NSManagedObjectContext *writeContext = self.writeContext;
    NSManagedObjectContext *mainContext = self.mainContext;
    
    [mainContext performBlock:^{
        NSError *error = nil;
        if ([mainContext hasChanges] && ![mainContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        
        [writeContext performBlock:^{
            NSError *error = nil;
            if ([writeContext hasChanges] && ![writeContext save:&error]) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }];
    }];
}

- (void)performBlock:(void(^)())block target:(id)target async:(BOOL)async {
    
    if (async) {
        [target performBlock:block];
    }
    else {
        [target performBlockAndWait:block];
    }
}

- (void)saveWriteContext
{
    NSManagedObjectContext *context = self.writeContext;
    [self saveContext:context];
}

- (void)saveMainContext
{
    NSManagedObjectContext *context = self.mainContext;
    [self saveContext:context];
}

- (void)saveContext:(NSManagedObjectContext *)context
{
    //You need the performBlock to execute the save on the Context Queue
    [self performBlock:^{
        NSError *error = nil;
        if ([context hasChanges] && ![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    } target:context async:YES];
}

- (NSManagedObjectContext *)newPrivateContext
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.parentContext = self.mainContext;
    context.undoManager = nil;
    return context;
}

#pragma mark - Save Notification

- (void)willSave:(NSNotification *)notification
{
    NSManagedObjectContext *context = notification.object;
    [self.delegate dataManager:self willSaveContext:context];
}

- (void)didSave:(NSNotification *)notification
{
    NSManagedObjectContext *saveContext = notification.object;
    NSManagedObjectContext *writeContext = self.writeContext;
    
    if (writeContext == saveContext) {
        NSLog(@"Core Data - Save write context.");
        return;
    }
    
    [saveContext performBlockAndWait:^{
        [saveContext.parentContext performBlockAndWait:^{
            [saveContext.parentContext mergeChangesFromContextDidSaveNotification:notification];
        }];
    }];
}

#pragma mark - Serious Error

- (void)_seriousError:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Core Data - Serious Error: %@", message);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Core Data" message:message delegate:self cancelButtonTitle:@"Close application!" otherButtonTitles:nil];
        alert.tag = kTagAlertSerious;
        [alert show];
    });
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kTagAlertSerious) {
        exit(0);
    }
}

@end

NSURL *DocDir()
{
    static NSURL *docDir = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
        docDir = [urls lastObject];
    });
    return docDir;
}

NSURL *CacheDir()
{
    static NSURL *cacheDir = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *urls = [fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
        cacheDir = [urls lastObject];
    });
    return cacheDir;
}
