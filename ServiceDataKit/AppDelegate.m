//
//  AppDelegate.m
//  ServiceDataKit
//
//  Created by ZenDo on 2/6/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "Test.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Test test];
    return YES;
}

@end
