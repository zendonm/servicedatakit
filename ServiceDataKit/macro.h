//
//  macro.h
//  Karaoke
//
//  Created by Nikmesoft on 11/4/13.
//  Copyright (c) 2013 Nikmesoft Co., Ltd. All rights reserved.
//

#ifdef DEBUG
#    define NSLog(fmt, ...) printf("➢ %s\n\n", [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String])
#else
#    define NSLog(fmt, ...) ;
#endif

#define m_device_type               ([[UIDevice currentDevice] userInterfaceIdiom])
#define m_device_type_iphone        (UIUserInterfaceIdiomPhone)
#define m_device_type_ipad          (UIUserInterfaceIdiomPad)
#define m_device_height             ([[UIScreen mainScreen] bounds].size.height)
#define m_system_version            (floor([[[UIDevice currentDevice] systemVersion] floatValue]))
#define m_string(str)               (NSLocalizedString(str, nil))