//
//  CDItem.h
//  ServiceDataKit
//
//  Created by ZenDo on 3/14/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CellDelegate <NSObject>
@required
- (void)_showDetailOfItemAtCell:(UITableViewCell *)cell;
@end

@interface ItemCell : UITableViewCell

@property (nonatomic, weak) id <CellDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIImageView *photo;
@property (nonatomic, weak) IBOutlet UILabel *sumary;

@end
