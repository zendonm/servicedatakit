//
//  Test.h
//  ServiceDataKit
//
//  Created by ZenDo on 2/9/14.
//  Copyright (c) 2014 Nikmesoft Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Test : NSObject

+ (void)test;

@end
